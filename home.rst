Home
###############################################################################

:slug: index
:modified: 2016-02-24 11:28

The SPICE project aims to provide a complete open source solution for remote
access to virtual machines in a seamless way so you can play videos, record
audio, share usb devices and share folders without complications.

.. image:: static/images/rhel7_win7.png
    :width: 48%
    :alt: RHEL7 client connected to a Windows guest
    :align: left
    :target: static/images/rhel7_win7.png
.. image:: static/images/win7_rhel.png
    :width: 48%
    :alt: Windows7 client connected to RHEL6 and RHEL7 guests
    :align: right
    :target: static/images/win7_rhel.png

SPICE could be divided into 4 different components: Protocol, Client, Server
and Guest. The protocol is the specification in the communication of the three
other components; A client such as remote-viewer is responsible to send data and
and translate the data from the Virtual Machine (VM) so you can interact with it;
The SPICE server is the library used by the hypervisor in order to share the VM
under SPICE protocol; And finally, the Guest side is all the software that must
be running in the VM in order to make SPICE fully functional, such as the QXL
driver and SPICE VDAgent.

.. image:: static/images/spice_schem.png
    :alt: scheme with SPICE components
    :align: center
    :target: static/images/spice_schem.png

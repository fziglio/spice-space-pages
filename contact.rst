Contact
###############################################################################

.. _spice-devel: http://lists.freedesktop.org/archives/spice-devel/
.. _Join spice-devel@lists.freedesktop.org: http://lists.freedesktop.org/mailman/listinfo/spice-devel
.. _Join spice-commits@lists.freedesktop.org: http://lists.freedesktop.org/mailman/listinfo/spice-commits
.. _spice-commits: http://lists.freedesktop.org/archives/spice-commits/
.. _Join spice-announce@lists.freedesktop.org: http://lists.freedesktop.org/mailman/listinfo/spice-announce
.. _spice-announce: http://lists.freedesktop.org/archives/spice-announce/
.. _irc.gimp.net #spice: irc://irc.gimp.net/spice

Spice developers list
+++++++++++++++++++++

- Post email to spice-devel@lists.freedesktop.org
- `Join spice-devel@lists.freedesktop.org`_
- Search `spice-devel`_ archive

Spice commit list
+++++++++++++++++

- `Join spice-commits@lists.freedesktop.org`_
- Search `spice-commits`_ archive

Spice announce list
+++++++++++++++++++

- `Join spice-announce@lists.freedesktop.org`_
- Search `spice-announce`_ archive

Spice irc channel
+++++++++++++++++

- `irc.gimp.net #spice`_
